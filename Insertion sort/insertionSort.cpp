void insertion(array, length) {
    int swapBuffer;
    for (int i = 1; i < length; i++) {
        int position = i;
        while (array[position] < array[position - 1] && position != 0) {
            swapBuffer = array[position];
            array[position] = array[position - 1];
            array[position - 1] = swapBuffer;
            position--;
        }
    }
}